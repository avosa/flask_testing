# flask_testing

[flask_testing](https://gitlab.com/avosa/flask_testing.git) is a Restful API with one endpoint, that performs various operations on an uploaded csv.

# Getting started

To start testing this microservice, you need to have [Python3](https://www.python.org/downloads/) and `requirements.txt dependancies` installed on your Mac/PC.

For testing purposes, use [Curl](https://curl.se/) 😎

# Steps

```sh
git clone https://gitlab.com/avosa/flask_testing.git
```

```sh
cd ~/flask_testing
```

```sh
virtualenv venv && source venv/bin/activate
```

```sh
pip3 install -r requirements.txt
```

```sh
flask run
```

### Run tests

```sh
python3 -m pytest -v
```

# Endpoints

# GET `/echo`

Test API endpoint `echo`

### 1. Success response

Correct CSV file uploaded.

```sh
curl -F 'file=@matrix.csv' http://127.0.0.1:5000/echo
```

### 2. Error responses

#### 2.1. Missing file

```sh
curl http://127.0.0.1:5000/echo
```

The above will return an error with the prompt: `Hello, please upload a csv file!`

#### 2.2. Wrong file format uploaded

```sh
curl -F 'file=@requirements.txt' http://127.0.0.1:5000/echo
```

The above command will return an error with the prompt: `Invalid CSV file`

Enjoy!

# Author

[Webster Avosa](https://github.com/avosa)
